#! /bin/bash
echo "Enter names : "
read name
echo "Entered name: $name"

#Another example

read name1 name2 name3
echo "Names: $name1, $name2, $name3"

#Input in the same line 

read -p 'username: ' user_val
echo "$user_val"

#Silent flag

read -sp 'pass: ' pass 

#Array input

echo "Enter names: "
read -a names
echo "${names[0]}, ${names[1]}"



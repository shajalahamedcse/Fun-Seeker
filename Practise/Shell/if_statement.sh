#! /bin/bash
#Basic Syntax of if statement

# if [condition]
# then 
#   statement
# fi


count=10
if [ $count -eq 10 ]
then
    echo "True"
elif [ $count -ne 11 ]
then 
    echo "False"
fi



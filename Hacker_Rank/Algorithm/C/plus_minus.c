#include <stdio.h>
int main()
{
    int i,N,input[100],counter_pos=0,counter_zero=0,counter_neg=0;
    float per_pos,per_neg,per_zero;
    scanf("%d",&N);
    for(i=0;i<N;i++)
    {
        scanf("%d",&input[i]);
        if(input[i]==0)
            counter_zero++;
        else {
            if(input[i]>0){
                counter_pos++;
            }
            else {
                counter_neg++;
            }
        }
    }
    per_pos=(float) counter_pos/N;
    per_neg=(float) counter_neg/N;
    per_zero=(float) counter_zero/N;
    printf("%.6f\n",per_pos);
    printf("%.6f\n",per_neg);
    printf("%.6f\n",per_zero);
    return 0;
}

read x
sum=0
for((i=1;i<=x;i++))
do
    read y
    sum=$((sum+y))
done
last=$(echo "scale=3; $sum/$x" | bc -l)
echo $last

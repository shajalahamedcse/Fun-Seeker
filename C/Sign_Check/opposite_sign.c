//Detect if two integers have opposite signs


#include<stdio.h>
int main()
{
    int x,y,z;
    int  f;
    printf("Enter two number :");
    scanf("%d %d",&x,&y); //input values to compare signs 
    z=x^y;
    printf("\n%d\n",z);
    f=z<0; //true iff x and y have opposite signs
    if(f)
    {
        printf("They have opposite  sign.\n");
    }
    else{
        printf("They have same sign.\n");
    }
    return 0;
}
